<h1 align="center"> 
	🚧 Readme em construção 🚧
</h1>

<img align='right' src="https://i.pinimg.com/originals/e4/26/70/e426702edf874b181aced1e2fa5c6cde.gif" width="230">

## ✅Bem vindo ao meu Github: Paulofreitas.py🚀  

👋Olá Mundo **Paulo Freitas**, tenho 22 anos e minha grande paixão deste os 17 é programar!!
atualmente sou estudante da Ciência da Computação, ingeressei no primeiro semestre de 2020, bebo bastante café, amo astronomia, cosmologia e astrofísica, sou bastante curioso em Data science, hacking, engenharia reversa e pentest. E nesse exato momento meu maior foco é começar a ser um profissional em Front-End (começando de baixo por que sonhos decolam aos poucos) 


## 🛠 Habilidades técnicas
- Python
- React
- Git
- MySQL / SQL
- HTML5
- Java Script



## ✅ Entre em contato!

<p align="center">
  <a href="https://www.linkedin.com/in/paulofreitas-py/">
    <img src="https://img.shields.io/badge/LinkedIn-0077B5?style=for-the-badge&logo=linkedin&logoColor=white"/>
  </a>
  <a href="https://github.com/paulofreitas-py">
    <img src="https://img.shields.io/badge/GitHub-100000?style=for-the-badge&logo=github&logoColor=white"/>
  </a>
</p>

## 💻Skills 
<p align="left">
  <br>
    <img src="https://github-readme-stats.vercel.app/api?username=paulofreitas-py&theme=dracula&bg_color=0D1117&title_color=3DDC84&icon_color=3DDC84&show_icons=true&hide_border=true" />
    <img src="https://github-readme-stats.vercel.app/api/top-langs/?username=paulofreitas-py&theme=dracula&bg_color=0D1117&title_color=3DDC84&layout=compact&hide=css,html&hide_border=true" />
  <br>
</p>
